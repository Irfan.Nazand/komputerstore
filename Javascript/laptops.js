export const Laptops = [
    {
        Id: 1,
        Name: 'HP Machine',
        Img: 'Images/HP.png',
        SSD: '256 GB',
        CPU: 'Core I7',
        Size: '13.4 Inches',
        Price: 11599
    },

    {
        Id: 2,
        Name: 'Lenovo Very Good',
        Img: 'Images/lenovo.png',
        SSD: '150 GB',
        CPU: 'Core I5',
        Size: '13.5 Inches',
        Price: 12300
    },

    {
        Id: 3,
        Name: 'Macbook overrated',
        Img: 'Images/macbook.png',
        SSD: '20 GB',
        CPU: 'MAC',
        Size: '15 Inches',
        Price: 19799
    },

    {
        Id: 4,
        Name: 'VERY EXPENSIVE',
        Img: 'Images/expensive.png',
        SSD: '500 GB',
        CPU: 'Core I9',
        Size: '14.5 Inches',
        Price: 122599
    }
]