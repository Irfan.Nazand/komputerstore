import {Laptops} from './laptops.js'

let elBalanceAmount = document.getElementById('balance-amount')
let elPayAmount = document.getElementById('pay-amount')
const elLoanBtn = document.getElementById('loan-btn')
const elBankBtn = document.getElementById('bank-btn')
const elWorkBtn = document.getElementById('work-btn')

const elSelectLaptop = document.getElementById('select-laptop')
const elLaptopImage = document.getElementById('laptop-image')
const elLaptopName = document.getElementById('laptop-name')
const elLaptopSsd = document.getElementById('laptop-ssd')
const elLaptopCpu = document.getElementById('laptop-cpu')
const elLaptopSize = document.getElementById('laptop-size')
const elLaptopPrice = document.getElementById('laptop-price')

const elBuyNowBtn = document.getElementById('buy-btn')

let balance = 0;
let pay = 0;
let applyForLoan = true;
let price = 0;

elSelectLaptop.addEventListener('change', function(event){
    const laptop = Laptops.find((currentlaptop) => {
        return currentlaptop.Id == event.target.value;
    })
    getLaptop(laptop)
})

function getLaptop(laptop) {
    elLaptopName.innerHTML = laptop.Name 
    elLaptopImage.src = laptop.Img
    elLaptopSsd.innerHTML = "SSD: " + laptop.SSD
    elLaptopCpu.innerHTML = "CPU: " + laptop.CPU
    elLaptopSize.innerHTML = "Laptop Size: " + laptop.Size
    elLaptopPrice.innerHTML = "PRICE!!!: " + laptop.Price
}

//Logic behind applying for loan
function applyLoan(amount){
    if (amount > 2*balance){
        window.alert('You cant apply for a loan more than double of your bank balance')
    }
    else if (!applyForLoan) {
        window.alert('You need to buy a laptop before applying for another loan')
    }
    else {
        balance = balance + amount
        elBalanceAmount.innerHTML = balance + "kr"
        window.alert(`Congratulations your application have been approved and you have received: ${amount}`)
        applyForLoan = false
    }
}

//Amount the user wants as loan
elLoanBtn.addEventListener('click', function() {
    let userInputLoan = parseInt(window.prompt('Enter amount. Remember you cant apply for more than twice your bank balance.'))
    applyLoan(userInputLoan)
});

// Work button to earn money
function earnMoney(){
    pay += 1000
    elPayAmount.innerHTML = pay + "kr";
}
elWorkBtn.addEventListener('click', earnMoney)


//Send money to bank balance
function sendMoneyToBank(){
    balance = balance + pay
    pay = 0
    elBalanceAmount.innerHTML = balance + "kr"
    elPayAmount.innerHTML = pay + "kr"
}
elBankBtn.addEventListener('click', sendMoneyToBank);


function buyLaptop(){

    price = elLaptopPrice.innerHTML
    const anotherPrice = price.match(/\d+/g).map(Number)[0];

    if (balance >= anotherPrice) 
    {
        balance = balance - anotherPrice
        elBalanceAmount.innerHTML = balance
        applyForLoan = true
        
        window.alert('Congratulations on your new LAPTOP')
    } 
    else if ( balance < anotherPrice) 
    {
        window.alert('You are poor, work more, earn more money or choose a cheaper laptop')
    }
}
//Buy laptop
elBuyNowBtn.addEventListener('click', function(){
    buyLaptop(price);
})

